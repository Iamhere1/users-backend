/* eslint-disable no-undef */
const axios = require("axios");
const { startExpressServer } = require("../src/express-server");
const { makeUserService } = require("../src/user/user.service");
const { getDb } = require("../src/db");
const { users } = require("./fixtures");

const BASE_URL = "http://localhost:3000";

describe("Users API", () => {
  let server;

  beforeAll(async () => {
    const db = await getDb();
    const userService = makeUserService({ getDatabase: getDb })
    await db.run(`CREATE TABLE IF NOT EXISTS "users" (
      "id" SERIAL NOT NULL,
      "originalId" integer NOT NULL,
      "email" character varying(255) NOT NULL,
      "avatar" character varying(510) NOT NULL,
      "firstName" character varying(255) NOT NULL,
      "lastName" character varying(255) NOT NULL,
      "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
      "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
      "page" integer NOT NULL,
      CONSTRAINT "PK_76acf48dc34f1c3525d71606cbc" PRIMARY KEY ("id")
    )`);
    await Promise.all(
      users.map(u => userService.insertIfNotExist({ ...u, page: 1 }))
    );
    const c = await startExpressServer();
    server = c[1];
  });

  afterAll(async () => {
    server.close();
    const db = await getDb();
    await db.shutdown()
    console.log('done');
  });

  describe("search users", () => {
    it("gets all users", async () => {
      const response = await axios.get(BASE_URL + `/users`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(users.length);
    });
    it("gets all users with name 'George'", async () => {
      const response = await axios.get(BASE_URL + `/users?q=George`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(1);
    });
    it("gets all users with name 'Charles Morris'", async () => {
      const response = await axios.get(BASE_URL + `/users?q=Charles%20Morris`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(1);
    });
    it("gets no users with name 'alalalal'", async () => {
      const response = await axios.get(BASE_URL + `/users?q=alalalal`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(0);
    });
  });
});
