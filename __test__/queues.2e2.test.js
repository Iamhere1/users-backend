/* eslint-disable no-undef */
const axios = require("axios");
const { startExpressServer } = require("../src/express-server");
const { makeUserService } = require("../src/user/user.service");
const { getDb } = require("../src/db");
const { users } = require("./fixtures");
const { makeUserQueues } = require("../src/user/user.queue");

const BASE_URL = "http://localhost:3000";
const sleep = delay => new Promise(res => setTimeout(res, delay));

describe("Users queues", () => {
  let getUsersQueue;
  let addUserQueue;
  let server;
  const mockUserService = makeUserService({
    client: () =>
      new Promise(resolve =>
        setImmediate(() => resolve({ page: 1, data: users }))
      ),
    getDatabase: getDb
  });
  beforeAll(async () => {
    const db = await getDb();
    await db.run('TRUNCATE TABLE "users"');
    const startUserQueues = makeUserQueues({
      userService: mockUserService,
      options: {}
    });
    const queues = await startUserQueues();
    getUsersQueue = queues.getUsersQueue;
    addUserQueue = queues.addUserQueue;
    connections = queues.connections;

    const c = await startExpressServer();
    server = c[1];
  });

  afterAll(async () => {
    await server.close();
    await getUsersQueue.close();
    await addUserQueue.close();
    await connections.forEach(c => c.quit());
    const db = await getDb();
    await db.shutdown();
    console.log("done");
  });

  describe("adds users to db", () => {
    it("gets no users before queue started", async () => {
      const response = await axios.get(BASE_URL + `/users`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(0);
    });

    it("gets all users with name 'George' after current jobs finishes", async () => {
      jest.setTimeout(20000);

      await sleep(100);
      let counter = 1;
      await addUserQueue.whenCurrentJobsFinished();
      while (counter !== 0) {
        const { waiting, active } = await addUserQueue.getJobCounts();
        counter = waiting || active;
      }

      const response = await axios.get(BASE_URL + `/users?q=George`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(1);
    });
    it("gets all users with name 'Charles Morris'  after current jobs finishes", async () => {
      const response = await axios.get(BASE_URL + `/users?q=Charles%20Morris`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(1);
    });
    it("gets no users with name 'alalalal' after current jobs finishes", async () => {
      const response = await axios.get(BASE_URL + `/users?q=alalalal`);
      expect(response.status).toEqual(200);
      expect(response.data.length).toEqual(0);
    });
  });
});
