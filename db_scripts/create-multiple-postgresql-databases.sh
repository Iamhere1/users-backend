#!/bin/bash

set -e
set -u

function create_user_and_database() {
	local database=$1
	echo "  Creating user and database '$database'"
	psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
	    CREATE USER $database;
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $database;
EOSQL
}

function create_user_table() {
	psql -v ON_ERROR_STOP=1 --username "$1" <<-EOSQL

CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL NOT NULL,
  "originalId" integer NOT NULL,
  "email" character varying(255) NOT NULL,
  "avatar" character varying(510) NOT NULL,
  "firstName" character varying(255) NOT NULL,
  "lastName" character varying(255) NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
  "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
  "page" integer NOT NULL,
  CONSTRAINT "PK_76acf48dc34f1c3525d71606cbc" PRIMARY KEY ("id")
)
EOSQL
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]; then
	echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		create_user_and_database $db
	done
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		create_user_table $db
	done
	echo "Multiple databases created"
fi