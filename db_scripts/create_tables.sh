#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL

CREATE TABLE IF NOT EXISTS "users" (
  "id" SERIAL NOT NULL,
  "originalId" integer NOT NULL,
  "email" character varying(255) NOT NULL,
  "avatar" character varying(510) NOT NULL,
  "firstName" character varying(255) NOT NULL,
  "lastName" character varying(255) NOT NULL,
  "createdAt" TIMESTAMP NOT NULL DEFAULT now(),
  "updatedAt" TIMESTAMP NOT NULL DEFAULT now(),
  "page" integer NOT NULL,
  CONSTRAINT "PK_76acf48dc34f1c3525d71606cbc" PRIMARY KEY ("id")
)
EOSQL