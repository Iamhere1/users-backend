const { startExpressServer } = require("./express-server");
const { startUserQueues } = require("./user");

(async function main() {
  startUserQueues();
  await startExpressServer();
})();
