const makeUserService = ({
  client,
  apiUrl = "https://reqres.in/api",
  getDatabase
} = {}) => {
  const columns = [
    '"originalId"',
    "email",
    '"firstName"',
    '"lastName"',
    "avatar",
    "page"
  ];
  const getFromApi = async ({ page = 1, limit = 10 } = {}) => {
    const options = {
      uri: apiUrl + `/users?page=${page}&per_page=${limit}`,
      json: true
    };

    return await client(options);
  };

  const getFromDb = async ({ search } = {}) => {
    const db = await getDatabase();
    let query = "SELECT * FROM users";
    const params = [];
    if (search) {
      query += ` WHERE concat(users."firstName", ' ', users."lastName") ILIKE $1`;
      params.push(`%${search}%`);
    }
    return await db.all(query, params);
  };

  const insertIfNotExist = async ({
    id,
    email,
    first_name,
    last_name,
    avatar,
    page
  }) => {
    const db = await getDatabase();
    const result =  await db.run(
      `
      INSERT INTO "users" (${columns.join(", ")})
      SELECT $1,$2,$3,$4,$5,$6
      WHERE (SELECT "originalId" FROM users WHERE "originalId" = $1 LIMIT 1) IS NULL
      RETURNING "originalId";
    `,
      [id, email, first_name, last_name, avatar, page]
    );
    return result.rows;
  };

  const getLastInserted = async () => {
    const db = await getDatabase();
    return await db.get(`
      SELECT "page" FROM users ORDER BY "originalId" DESC LIMIT 1
    `);
  };

  // yield users one by one from start page to the end
  async function* fetchUsersGenerator({ page = 1 } = {}) {
    let total_pages = 0;
    while (page <= total_pages || total_pages === 0) {
      const res = await getFromApi({ page });
      const { data } = res;
      total_pages = res.total_pages || 0;
      for (const user of data) {
        user.page = page;
        yield user;
      }
      page += 1;

      if (page === total_pages + 1 || total_pages === 0) return;
    }
  }

  return {
    getFromApi,
    getFromDb,
    insertIfNotExist,
    getLastInserted,
    fetchUsersGenerator
  };
};

exports.makeUserService = makeUserService;
