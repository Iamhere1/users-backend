const promiseRouter = require("express-promise-router");


const makeUserRouter = ({ userController }) => {
  const router = promiseRouter();

  router.get("/", userController.getAll);
  return router;
};

exports.makeUserRouter = makeUserRouter;
