const rp = require("request-promise");
const { getDb } = require("../db");
const { makeUserService } = require("./user.service");
const { makeUserController } = require("./user.controller");
const { makeUserRouter } = require("./user.router");
const { makeUserQueues } = require("./user.queue");

const userService = makeUserService({ client: rp, getDatabase: getDb });
const userController = makeUserController({ userService });
const userRouter = makeUserRouter({ userController });
const startUserQueues = makeUserQueues({ userService });

exports.userService = userService;
exports.userRouter = userRouter;
exports.userRouter = userRouter;
exports.startUserQueues = startUserQueues;
