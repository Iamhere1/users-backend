const Queue = require("bull");
const Redis = require("ioredis");
const { setQueues } = require("bull-board");
const { logger } = require("../log");


const makeUserQueues = ({
  userService,
  options = { repeat: { cron: "* * * * *" /* every minute */ } }
} = {}) => () => {

  const client = new Redis();
  const subscriber = new Redis();
  const connections = [client, subscriber];

  const opts = {
    createClient(type) {
      switch (type) {
        case "client":
          return client;
        case "subscriber":
          return subscriber;
        default: {
          const redis = new Redis();
          connections.push(redis);
          return redis;
        }
      }
    }
  };

  const getUsersQueue = new Queue("get new users from api", opts);
  const addUserQueue = new Queue("add new users to db", opts);

  getUsersQueue.add({}, options);

  getUsersQueue.process(async () => {
    try {
      const user = await userService.getLastInserted();
      const { page } = user || {}; // last fetched page, user can be null at first
      let counter = 0;
      // fetch starting from last fetched page
      for await (const item of userService.fetchUsersGenerator({ page })) {
        addUserQueue.add(item);
        counter++;
      }
      // fetched items
      return counter;
    } catch (error) {
      logger.error(`Error while fetching users: ${error}`);
    }
  });
  getUsersQueue.on("completed", (job, count) => {
    logger.log(`Fetched ${count} users at ${new Date().toISOString()}`);
  });

  addUserQueue.process(async result => {
    const user = result.data;
    if (Object.keys(user).length !== 0) {
      return userService.insertIfNotExist(user);
    }
    return;
  });
  addUserQueue.on("completed", (job, { data: id }) => {
    if (id) {
      logger.log(`Added user with id: ${id} at ${new Date().toISOString()}`);
    }
  });
  setQueues([getUsersQueue, addUserQueue]);
  return { getUsersQueue, addUserQueue, connections };
};

exports.makeUserQueues = makeUserQueues;
