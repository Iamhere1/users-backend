const makeUserController = ({ userService }) => {
  const getAll = async (req, res) => {
    const search = req.query.q;
    const users = await userService.getFromDb({ search });
    return res.status(200).json(users);
  }
  return {
    getAll
  }
}

exports.makeUserController = makeUserController;
