const path = require("path");
const result = require("dotenv").config({
  path: path.resolve(
    process.cwd(),
    process.env.NODE_ENV ? process.env.NODE_ENV + ".env" : "development.env"
  ),
});
if (result.error) {
  throw result.error;
}

module.exports = {
  database: process.env.database,
  host: process.env.host,
  port: process.env.port,
  schema: process.env.schema,
  user: process.env.user,
  password: process.env.password,
};
