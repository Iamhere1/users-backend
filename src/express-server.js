const bodyParser = require("body-parser");
const express = require("express");
const { logger } = require("./log");
const { router } = require("./router");
const cors = require("cors");
const { UI } = require("bull-board");

async function startListening(app, PORT) {
  return new Promise(res => {
    const server = app.listen(PORT, () => {
      logger.info(`Server listening on PORT:${PORT}`);
      res(server);
    });
  });
}

async function setupRouting(app) {
  app.use(router);
}

async function setupDevMiddleware(app) {
  app.enable("x-powered-by");
}

async function setupProdMiddleware(app) {
  app.disable("x-powered-by");
}

exports.startExpressServer = async function startExpressServer() {
  const app = express();

  // parse application/json
  app.use(bodyParser.json());
  app.use(cors());
  if (process.env.NODE_ENV !== "test") {
    app.use("/admin/queues", UI);
  }
  if (process.env.NODE_ENV !== "prod") {
    await setupDevMiddleware(app);
  } else {
    await setupProdMiddleware(app);
  }

  await setupRouting(app);

  const server = await startListening(app, process.env.PORT || 3000);
  return [app, server];
};
