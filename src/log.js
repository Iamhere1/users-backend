exports.logger = {
  debug: (...args) => console.log(...args),
  verbose: (...args) => console.log(...args),
  info: (...args) => console.log(...args),
  warn: (...args) => console.error(...args),
  error: (...args) => console.error(...args),
};
