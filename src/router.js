const { userRouter } = require("./user");
const router = require("express-promise-router")();

router.use("/users", userRouter);

exports.router = router;
