const pg = require("pg");
const config = require("../config");
const { logger } = require("../log");

function timeVal(ms) {
  if (ms < 1) {
    return `${(ms * 1000).toPrecision(4)}μs`;
  } else if (ms < 1000) {
    return `${ms.toPrecision(4)}ms`;
  }
  return `${(ms / 1000).toPrecision(4)}s`;
}

const pool = (function() {
  const { database, host, port, schema, user, password } = config;
  const p = new pg.Pool(
    process.env.DATABASE_URL
      ? { connectionString: process.env.DATABASE_URL }
      : {
          database,
          user,
          password,
          host,
          port,
        }
  );
  if (process.env.NODE_ENV !== "test") {
    logger.info(
      process.env.DATABASE_URL
        ? `Creating database pool for ${process.env.DATABASE_URL}`
        : `Creating database pool for postgres://${user}@${host}:${port}#${database}.${schema}`
    );
  }
  return p;
})();

pool.on("error", err => {
  logger.error("Unexpected error on idle client", err);
  // eslint-disable-next-line no-process-exit
  process.exit(-1);
});

class Db {
  static async setup() {
    const client = await pool.connect();
    try {
      return new this(client);
    } catch (e) {
      logger.error(`ERROR during posgres setup\n${e}`);
      throw e;
    } finally {
      client.release();
    }
  }

  constructor(client) {
    this.client = client;
  }

  async shutdown() {
    await pool.end();
  }

  async measure(query, params, fn) {
    const begin = process.hrtime();
    try {
      const result = await fn();
      const end = process.hrtime();
      const diff = [end[0] - begin[0], end[1] - begin[1]];
      const t = diff[0] * 1000 + diff[1] / 1000000;
      if (process.env.NODE_ENV !== "test") {
        this.logQuery(query, params, t);
      }
      return result;
    } catch (e) {
      logger.error(`Problem running query
      ${query}
      PARAMS: ${JSON.stringify(params)}
      'ERROR:' ${e.toString()}`);
      throw e;
    }
  }

  logQuery(query, params, t) {
    const tv = timeVal(t);
    const timeString = tv;
    logger.info(
      [query + ` (${timeString})`, `PARAMS: ${JSON.stringify(params)}`].join(
        "\n"
      )
    );
    return t;
  }

  async run(query, params) {
    
    return this.measure(query, params, async () => {
     return await this.client.query(query, params);
    });
  }

  async get(query, params) {
    return this.measure(
      query,
      params,
      async () =>
        await this.client.query(query, params).then(result => result.rows[0])
    );
  }

  async all(query, params) {
    return this.measure(
      query,
      params,
      async () =>
        await this.client.query(query, params).then(result => result.rows)
    );
  }
}

async function getDb() {
  return await Db.setup();
}

async function getPool() {
  return pool;
}

exports.getPool = getPool;
exports.getDb = getDb;
exports.Db = Db;
