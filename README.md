###To start the app 
- docker and node.js >= 12.13 must be installed.
#### Install dependencies
```sh
npm install
```
#### Run docker
```sh
docker-compose up
```
#### In another tab run the server
- for development
```sh
npm run dev
```
### For 2e2 tests
#### Run docker
```sh
docker-compose up
```
#### Run tests
```sh
npm run test
```


